describe package('docker-ce') do
  it { should be_installed }
end

describe service('docker') do
  it { should be_running }
end

describe directory('/etc/docker/tls') do
  its('mode') { should cmp 0o700 }
  its('owner') { should eq 'root' }
end

describe docker_service('up_web') do
  it { should exist }
  its('ports') { should include '*:80->8080'}
end

describe http('http://localhost') do
  its('body') { should cmp 'Hello World'}
end
