output ssh_private_key {
  value = "${tls_private_key.ssh_private_key.private_key_pem}"
  sensitive = true
}

output ssh_public_key {
  value = "${tls_private_key.ssh_private_key.public_key_openssh}"
}

output dns_name {
  value = "${aws_instance.docker_master.public_dns}"
}

output ip_address {
  value = "${aws_instance.docker_master.public_ip}"
}
