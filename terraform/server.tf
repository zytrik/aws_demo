resource "aws_instance" "docker_master" {
    connection {
      user = "ubuntu"
      private_key = "${tls_private_key.ssh_private_key.private_key_pem}"
    }

    instance_type   = "${var.instance_type}"
    ami             = "${var.ami}"
    key_name        = "${aws_key_pair.docker.id}"
    vpc_security_group_ids =
      ["${aws_security_group.default.id}"]
    subnet_id       = "${aws_subnet.default.id}"


}

data "template_file" "setup_script" {
  template = "${file("scripts/docker_setup.sh")}"

  vars {
    ca_cert     = "${tls_self_signed_cert.docker_ca.cert_pem}"
    server_cert = "${tls_locally_signed_cert.docker_server.cert_pem}"
    server_key  = "${tls_private_key.docker_server.private_key_pem}"
    internal_ip  = "${aws_instance.docker_master.private_ip}"
  }
}

resource "null_resource" "docker_master_setup" {
  triggers {
    instance_id = "${aws_instance.docker_master.id}"
    ca_cert     = "${tls_self_signed_cert.docker_ca.cert_pem}"
    server_cert = "${tls_locally_signed_cert.docker_server.cert_pem}"
    server_key  = "${tls_private_key.docker_server.private_key_pem}"
  }

  connection {
    user = "ubuntu"
    private_key = "${tls_private_key.ssh_private_key.private_key_pem}"
    host = "${aws_instance.docker_master.public_ip}"
  }

  provisioner "file" {
    content     = "${data.template_file.setup_script.rendered}"
    destination = "docker_setup.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo bash ~ubuntu/docker_setup.sh",
      "rm ~ubuntu/docker_setup.sh",
    ]
  }

}
