variable "access_key" {}
variable "secret_key" {}
variable "region" {
  default = "us-east-1"
}
variable "ami" {
  default = "ami-6dfe5010"
}
variable "instance_type" {
  default = "t2.nano"
}
