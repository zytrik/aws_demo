#!/usr/bin/env bash

echo "Doing docker setup..."

mkdir -p /etc/docker/tls

cat << SSLCERT > /etc/docker/tls/ca.pem
${ca_cert}
SSLCERT

cat << SSLCERT > /etc/docker/tls/cert.pem
${server_cert}
SSLCERT

cat << SSLCERT > /etc/docker/tls/key.pem
${server_key}
SSLCERT

chmod -R go= /etc/docker/tls

cat << JSON > /etc/docker/daemon.json
{
    "hosts": ["unix:///var/run/docker.sock", "tcp://0.0.0.0:2376"],
    "tlscacert": "/etc/docker/tls/ca.pem",
    "tlscert": "/etc/docker/tls/cert.pem",
    "tlskey": "/etc/docker/tls/key.pem",
    "tlsverify": true
}
JSON

mkdir /etc/systemd/system/docker.service.d

cat <<SYSTEMD > /etc/systemd/system/docker.service.d/no_hosts.conf
[Service]
ExecStart=
ExecStart=/usr/bin/dockerd
SYSTEMD

apt-get update

apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -

add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

apt-get update

apt-get install -y docker-ce

docker swarm init --advertise-addr ${internal_ip} || true
