resource "tls_private_key" "ssh_private_key" {
    algorithm = "RSA"
}

resource "tls_private_key" "docker_ca" {
    algorithm = "RSA"
}

resource "tls_self_signed_cert" "docker_ca" {
    key_algorithm = "${tls_private_key.docker_ca.algorithm}"
    private_key_pem = "${tls_private_key.docker_ca.private_key_pem}"

    subject {
      common_name = "Docker Test CA"
      organization = "co6 Testing Ground"
    }

    validity_period_hours = "${ 24 * 366 }"

    is_ca_certificate = true

    allowed_uses = [
      "cert_signing",
  ]
}

resource "tls_private_key" "docker_server" {
    algorithm = "RSA"
}

resource "tls_cert_request" "docker_server" {
    key_algorithm = "${tls_private_key.docker_server.algorithm}"
    private_key_pem = "${tls_private_key.docker_server.private_key_pem}"

    subject {
      common_name = "${aws_instance.docker_master.public_dns}"
      organization = "co6 Testing Ground"
    }

    dns_names = [
      "${aws_instance.docker_master.public_dns}",
      "${aws_instance.docker_master.public_ip}",
      "${aws_instance.docker_master.private_dns}",
      "${aws_instance.docker_master.private_ip}",
    ]
}

resource "tls_locally_signed_cert" "docker_server" {
    cert_request_pem   = "${tls_cert_request.docker_server.cert_request_pem}"
    ca_key_algorithm   = "${tls_private_key.docker_ca.algorithm}"
    ca_private_key_pem = "${tls_private_key.docker_ca.private_key_pem}"
    ca_cert_pem        = "${tls_self_signed_cert.docker_ca.cert_pem}"

    validity_period_hours = "${ 24 * 365 }"

    allowed_uses = [
      "key_encipherment",
      "digital_signature",
      "server_auth",
    ]
}

resource "tls_private_key" "docker_client" {
    algorithm = "RSA"
}

resource "tls_cert_request" "docker_client" {
    key_algorithm = "${tls_private_key.docker_client.algorithm}"
    private_key_pem = "${tls_private_key.docker_client.private_key_pem}"

    subject {
      common_name = "docker_client"
      organization = "co6 Testing Ground"
    }
}

resource "tls_locally_signed_cert" "docker_client" {
    cert_request_pem   = "${tls_cert_request.docker_client.cert_request_pem}"
    ca_key_algorithm   = "${tls_private_key.docker_ca.algorithm}"
    ca_private_key_pem = "${tls_private_key.docker_ca.private_key_pem}"
    ca_cert_pem        = "${tls_self_signed_cert.docker_ca.cert_pem}"

    validity_period_hours = "${ 24 * 365 }"

    allowed_uses = [
      "client_auth",
    ]
}

resource "local_file" "ssh_private_key" {
  content  = "${tls_private_key.ssh_private_key.private_key_pem}"
  filename = "${path.root}/generated/id_rsa"
}

resource "local_file" "docker_ca_cert" {
    content  = "${tls_self_signed_cert.docker_ca.cert_pem}"
    filename = "${path.root}/generated/docker/ca.pem"
}

resource "local_file" "docker_client_cert" {
    content  = "${tls_locally_signed_cert.docker_client.cert_pem}"
    filename = "${path.root}/generated/docker/cert.pem"
}

resource "local_file" "docker_client_key" {
    content  = "${tls_private_key.docker_client.private_key_pem}"
    filename = "${path.root}/generated/docker/key.pem"
}
