# Quick Start
1. Run ./configure to initialize the Terraform repository and install inspec
2. Provide AWS credentials by editing `standup` or exporting `TF_VAR_access_key`
 and `TF_VAR_secret_key` in your shell.
3. Run ./standup to deploy
4. Run ./teardown to destroy after testing

Run `terraform output` from the terraform directory to get instance information.
Use the ssh key saved in `terraform/generated/id_rsa` for authentication.

# Requirements
The following software versions are required locally on the host performing the
provisioning. Other versions will probably work, but no backtesting was
performed on this deployment pipeline.

- Docker Client 18.04.0-ce
- Bundler (to install inspec)
- Terraform 0.11.7

# Assumptions

- Single command deployment, with separate environment setup
- Keep operational steps in functions that could be easily pipelined
- Only deploy a single web app that generates receivable data

The intent was to deploy infrastructure using standard OS images to AWS. No
layers were created for static or persistent content, but the Docker Stack
architecture used is easily extended.

Lateral scaling was not a priority either, however a single node swarm is
created by default which could easily be extended with some additional terraform
resources.

# Shortcomings
- A docker swarm "master" node is created, but cannot currently be scaled out
without some additional TF code.
- Additional swarm nodes are also prohibited without a central docker registry.
- Because everything is in a single repository, the web app and corresponding
docker image are not versioned, so rolling updates might not work as expected.
